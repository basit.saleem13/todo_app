import 'dart:convert';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:mockito/annotations.dart';
import 'package:morphosis_flutter_demo/non_ui/repo/home/home_repository.dart';

@GenerateMocks([http.Client])
main() {
  test("Testing the network call", () async {
    //setup the test
    final homeRepoProvider = HomeRepository();
    MockClient((request) async {
      final mapJson = {'id': 1};
      return Response(json.encode(mapJson), 200);
    });
    final item = await homeRepoProvider.getFacts();
    expect(item[0].id, 1);
  });
}
