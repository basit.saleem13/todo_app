import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:morphosis_flutter_demo/main.dart';
import 'package:morphosis_flutter_demo/ui/screens/home/index.dart';
import 'package:morphosis_flutter_demo/ui/screens/task/task.dart';

void main() {
  testWidgets('Main UI Test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(App());

    expect(find.byType(IndexPage), findsOneWidget);
  });

  MaterialApp app = MaterialApp(
    home: Scaffold(body: TaskPage()),
  );

  testWidgets('add/Edit task UI Test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(app);

    // expect(find.byType(TextField), findsOneWidget);
    expect(find.byType(CupertinoSwitch), findsOneWidget);
    expect(find.byType(ElevatedButton), findsOneWidget);
  });
}
