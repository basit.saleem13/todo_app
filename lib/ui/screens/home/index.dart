import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:morphosis_flutter_demo/non_ui/modal/task.dart';
import 'package:morphosis_flutter_demo/non_ui/repo/firebase_manager.dart';
import 'package:morphosis_flutter_demo/ui/screens/home/home.dart';
import 'package:morphosis_flutter_demo/ui/screens/task/bloc/task_bloc.dart';
import 'package:morphosis_flutter_demo/ui/screens/task/tasks.dart';

class IndexPage extends StatefulWidget {
  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  int _currentIndex = 0;

  List<Task> allTasks = [];
  List<Task> allCompletedTasks = [];

  @override
  void initState() {
    context.read<TaskBloc>().add(FetchAllTasks());
    super.initState();
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<TaskBloc, TaskState>(listener: (context, state) {
      if (state is FetchedAllTasks) {
        allTasks = state.tasks;
        allCompletedTasks =
            allTasks.where((element) => element.completedAt != null).toList();
      } else if (state is TaskErrorState) {
        showDialog(
            context: context,
            builder: (ctxt) =>
            new AlertDialog(
              title: Text("Oops!"),
              content: Text(state.error),
            )
        );
      }
    }, builder: (context, state) {
      return Scaffold(
        body: IndexedStack(
          children: <Widget>[
            HomePage(),
            TasksPage(
              title: 'All Tasks',
              tasks: allTasks,
            ),
            TasksPage(
              title: 'Completed Tasks',
              tasks: allCompletedTasks,
            )
          ],
          index: _currentIndex,
        ),
        bottomNavigationBar: BottomNavigationBar(
          onTap: onTabTapped,
          currentIndex: _currentIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.list),
              label: 'All Tasks',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.check),
              label: 'Completed Tasks',
            ),
          ],
        ),
      );
    });
  }
}
