import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:morphosis_flutter_demo/non_ui/modal/album.dart';
import 'package:morphosis_flutter_demo/non_ui/repo/home/home_repository.dart';
import 'package:morphosis_flutter_demo/non_ui/utils/preference_helper.dart';
import 'package:morphosis_flutter_demo/ui/screens/home/bloc/home_bloc.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return BlocProvider(
      create: (_) => HomeBloc()..add(FetchFactsEvent()),
      child: HomeUI(),
    );
  }
}

class HomeUI extends StatefulWidget {
  @override
  _HomeUIState createState() => _HomeUIState();
}

class _HomeUIState extends State<HomeUI> {
  TextEditingController _searchTextField = TextEditingController();
  List<Album> facts;
  List<Album> _searchResult = [];

  @override
  void initState() {
    _searchTextField.text = "";
    super.initState();
    getSavedFacts();
  }

  @override
  void dispose() {
    _searchTextField.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return BlocConsumer<HomeBloc, HomeState>(listener: (context, state) {
      if (state is FactsLoadedState) {
        facts = state.facts;
      }
    }, builder: (context, state) {
      return Scaffold(
        appBar: AppBar(
          title: Text("Home"),
          actions: [],
        ),
        body: Container(
          padding: EdgeInsets.all(15),
          height: size.height,
          width: size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              /* In this section we will be testing your skills with network and local storage. You need to fetch data from any open source api from the internet.
               E.g:
               https://any-api.com/
               https://rapidapi.com/collection/best-free-apis?utm_source=google&utm_medium=cpc&utm_campaign=Beta&utm_term=%2Bopen%20%2Bsource%20%2Bapi_b&gclid=Cj0KCQjw16KFBhCgARIsALB0g8IIV107-blDgIs0eJtYF48dAgHs1T6DzPsxoRmUHZ4yrn-kcAhQsX8aAit1EALw_wcB
               Implement setup for network. You are free to use package such as Dio, Choppper or Http can ve used as well.
               Upon fetching the data try to store thmm locally. You can use any local storeage.
               Upon Search the data should be filtered locally and should update the UI.
              */

              CupertinoSearchTextField(
                controller: _searchTextField,
                onChanged: (text) => onSearchTextChanged(text),
              ),
              Expanded(
                child: state is ErrorState
                    ? Center(
                        child: Text(
                          '${state.error}',
                          softWrap: true,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 3,
                          style: TextStyle(color: Colors.black),
                        ),
                      )
                    : state is NoFactFoundState
                        ? Center(
                            child: Text(
                              'No Data Found :(',
                              softWrap: true,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 3,
                              style: TextStyle(color: Colors.black),
                            ),
                          )
                        : state is LoadingState
                            ? Center(
                                child: Text(
                                  'Loading...',
                                  softWrap: true,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 3,
                                  style: TextStyle(color: Colors.black),
                                ),
                              )
                            : _searchResult.length != 0 ||
                                    _searchTextField.text.isNotEmpty
                                ? ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: _searchResult != null &&
                                            _searchResult.length > 0
                                        ? _searchResult.length
                                        : 0,
                                    itemBuilder: (context, index) {
                                      return Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: ListTile(
                                          title: Text(
                                            '${_searchResult[index].title}',
                                            softWrap: true,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 3,
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                        ),
                                      );
                                    })
                                : ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: facts != null && facts.length > 0
                                        ? facts.length
                                        : 0,
                                    itemBuilder: (context, index) {
                                      return Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: ListTile(
                                          title: Text(
                                            '${facts[index].title}',
                                            softWrap: true,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 3,
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                        ),
                                      );
                                    }),
              )
            ],
          ),
        ),
      );
    });
  }

  getSavedFacts() async {
    facts = await PreferencesHelper.getFacts();
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    if (facts != null) {
      facts.forEach((fact) {
        if (fact.title.toLowerCase().contains(text.toLowerCase()))
          _searchResult.add(fact);
      });

      setState(() {});
    }
  }
}
