import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:morphosis_flutter_demo/non_ui/modal/album.dart';
import 'package:morphosis_flutter_demo/non_ui/repo/home/home_repository.dart';
import 'package:morphosis_flutter_demo/non_ui/repo/service_locator.dart';
import 'package:morphosis_flutter_demo/non_ui/utils/helper_functions.dart';
import 'package:morphosis_flutter_demo/non_ui/utils/preference_helper.dart';

part 'home_event.dart';

part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {

  HomeBloc() : super(HomeInitial());

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if (event is FetchFactsEvent) {
      yield* _mapEventFetchFactsToState(event);
    }
  }

  Stream<HomeState> _mapEventFetchFactsToState(FetchFactsEvent event) async* {
    yield LoadingState();
    try {
      if (await isInternetConnected()) {
        var facts = await getIt.get<HomeRepository>().getFacts();
        if (facts != null && facts.length > 0) {
          yield FactsLoadedState(facts: facts);
          // saving facts locally here
          await PreferencesHelper.saveFacts(facts);
        } else {
          yield NoFactFoundState();
        }
      } else {
        var savedFacts = await PreferencesHelper.getFacts();
        if (savedFacts != null) {
          yield FactsLoadedState(facts: savedFacts);
        } else {
          yield ErrorState(
              error: 'No internet found! Please try run the app again');
        }
      }
    } catch (e) {
      yield ErrorState(error: e.toString());
    }
  }
}
