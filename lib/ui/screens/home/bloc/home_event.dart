part of 'home_bloc.dart';

@immutable
abstract class HomeEvent {}

class FetchFactsEvent extends HomeEvent {}

class SearchFactsEvent extends HomeEvent {
  final String text;

  SearchFactsEvent({this.text});
}
