part of 'task_bloc.dart';

@immutable
abstract class TaskEvent {}

class AddTaskEvent extends TaskEvent {
  final Task task;

  AddTaskEvent({this.task});
}

class FetchAllTasks extends TaskEvent {}

class FetchCompletedTasks extends TaskEvent {}

class TasksFetchedEvent extends TaskEvent {
  final List<Task> tasks;

  TasksFetchedEvent({this.tasks});
}

class DeleteTaskEvent extends TaskEvent {
  final Task task;

  DeleteTaskEvent({this.task});
}

class UpdateTaskEvent extends TaskEvent {
  final Task task;

  UpdateTaskEvent({this.task});
}
