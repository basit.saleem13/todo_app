part of 'task_bloc.dart';

@immutable
abstract class TaskState {}

class TaskInitial extends TaskState {}

class AddingTaskState extends TaskState {}

class TaskCreatedState extends TaskState {}

class TaskErrorState extends TaskState {
  final String error;

  TaskErrorState({this.error});
}

class FetchedAllTasks extends TaskState {
  final List<Task> tasks;

  FetchedAllTasks({this.tasks});
}

class CompletedTasksFetchedState extends TaskState {
  final List<Task> tasks;

  CompletedTasksFetchedState({this.tasks});
}

class NoTaskFoundState extends TaskState {}
