import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:morphosis_flutter_demo/non_ui/modal/task.dart';
import 'package:morphosis_flutter_demo/non_ui/repo/service_locator.dart';
import 'package:morphosis_flutter_demo/non_ui/repo/task/task_repositoy.dart';
part 'task_event.dart';

part 'task_state.dart';

class TaskBloc extends Bloc<TaskEvent, TaskState> {

  TaskBloc() : super(TaskInitial());

  @override
  Stream<TaskState> mapEventToState(
    TaskEvent event,
  ) async* {
    if (event is AddTaskEvent) {
      yield* _mapEventAddTaskToState(event);
    } else if (event is FetchAllTasks) {
      yield* _mapEventFetchAllTasksToState(event);
    } else if (event is TasksFetchedEvent) {
      yield* _mapEventFetchedAllTasksToState(event);
    } else if (event is DeleteTaskEvent) {
      yield* _mapEventDeleteTaskToState(event);
    } else if (event is UpdateTaskEvent) {
      yield* _mapEventUpdateTaskToState(event);
    }
  }

  Stream<TaskState> _mapEventAddTaskToState(AddTaskEvent event) async* {
    yield AddingTaskState();
    try {
      await getIt.get<TaskRepository>().addTask(event.task);
      yield TaskCreatedState();
    } catch (e) {
      yield TaskErrorState(error: e.toString());
    }
  }

  Stream<TaskState> _mapEventFetchAllTasksToState(FetchAllTasks event) async* {
    try {
      getIt.get<TaskRepository>().fetchAllTasks().listen((tasks) {
        add(TasksFetchedEvent(tasks: tasks));
      });
    } catch (e) {
      yield TaskErrorState(error: e.toString());
    }
  }

  Stream<TaskState> _mapEventFetchedAllTasksToState(
      TasksFetchedEvent event) async* {
    if (event.tasks.length < 0) {
      yield NoTaskFoundState();
    } else {
      yield FetchedAllTasks(tasks: event.tasks);
    }
  }

  Stream<TaskState> _mapEventDeleteTaskToState(DeleteTaskEvent event) async* {
    try {
      await getIt.get<TaskRepository>().removeTask(event.task);
    } catch (e) {
      yield TaskErrorState(error: e.toString());
    }
  }

  Stream<TaskState> _mapEventUpdateTaskToState(UpdateTaskEvent event) async* {
    try {
      await getIt.get<TaskRepository>().updateTask(event.task);
    } catch (e) {
      yield TaskErrorState(error: e.toString());
    }
  }
}
