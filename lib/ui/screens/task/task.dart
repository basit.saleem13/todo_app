import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:morphosis_flutter_demo/non_ui/modal/task.dart';
import 'package:morphosis_flutter_demo/ui/screens/task/bloc/task_bloc.dart';

class TaskPage extends StatelessWidget {
  TaskPage({this.task});

  final Task task;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(task == null ? 'New Task' : 'Edit Task'),
      ),
      body: _TaskForm(task),
    );
  }
}

class _TaskForm extends StatefulWidget {
  _TaskForm(this.task);

  final Task task;

  @override
  __TaskFormState createState() => __TaskFormState(task);
}

class __TaskFormState extends State<_TaskForm> {
  static const double _padding = 16;

  __TaskFormState(this.task);

  Task task;
  TextEditingController _titleController;
  TextEditingController _descriptionController;

  void init() {
    task = widget.task;
    if (task == null) {
      task = Task();
      _titleController = TextEditingController();
      _descriptionController = TextEditingController();
      task.title = _titleController.text;
      task.description = _descriptionController.text;
    } else {
      _titleController = TextEditingController(text: task.title);
      _descriptionController = TextEditingController(text: task.description);
    }
  }

  @override
  void initState() {
    init();
    super.initState();
  }

  void _save(BuildContext context) {
    if (isValid()) {
      BlocProvider.of<TaskBloc>(context).add(AddTaskEvent(
          task: Task(
              title: _titleController.text,
              description: _descriptionController.text,
              completedAt: task.completedAt)));
      Navigator.of(context).pop();
    } else {
      showDialog(
          context: context,
          builder: (ctxt) =>
          new AlertDialog(
            title: Text("Oops!"),
            content: Text("Empty fields not allowed"),
          )
      );
    }
  }

  void _update(BuildContext context) {
    if (isValid()) {
      BlocProvider.of<TaskBloc>(context).add(UpdateTaskEvent(
          task: Task(
              id: task.id,
              title: _titleController.text,
              description: _descriptionController.text,
              completedAt: task.completedAt)));
      Navigator.of(context).pop();
    } else {
      showDialog(
          context: context,
          builder: (ctxt) =>
          new AlertDialog(
            title: Text("Oops!"),
            content: Text("Empty fields not allowed"),
          )
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        padding: const EdgeInsets.all(_padding),
        child: Column(
          children: [
            TextField(
              controller: _titleController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Title',
              ),
            ),
            SizedBox(height: _padding),
            TextField(
              controller: _descriptionController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Description',
              ),
              minLines: 5,
              maxLines: 10,
            ),
            SizedBox(height: _padding),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Completed ?'),
                CupertinoSwitch(
                  value: task.isCompleted,
                  onChanged: (_) {
                    setState(() {
                      task.toggleComplete();
                    });
                  },
                ),
              ],
            ),
            Spacer(),
            ElevatedButton(
              onPressed: () => task.isNew ? _save(context) : _update(context),
              child: Container(
                width: double.infinity,
                child: Center(child: Text(task.isNew ? 'Create' : 'Update')),
              ),
            )
          ],
        ),
      ),
    );
  }

  bool isValid() {
    if (_titleController.text == null || _titleController.text.isEmpty)
      return false;
    else if (_descriptionController.text == null || _descriptionController.text.isEmpty) return false;
    return true;
  }
}
