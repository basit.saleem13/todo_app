import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:morphosis_flutter_demo/non_ui/modal/task.dart';
import 'package:morphosis_flutter_demo/non_ui/repo/firebase_manager.dart';

class TaskRepository {
  Future<void> addTask(Task task) async {
    DocumentReference ref =
        FirebaseManager.shared.firestore.collection("tasks").doc();
    String myId = ref.id;
    task.id = myId;
    await FirebaseManager.shared.firestore
        .collection('tasks')
        .doc(myId)
        .set(task.toJson());
  }

  Stream<List<Task>> fetchAllTasks() {
    return FirebaseManager.shared.firestore.collection("tasks").snapshots().map(
          (snap) => snap.docs.map((doc) => Task.fromJson(doc.data())).toList(),
        );
  }

  Future<void> removeTask(Task task) async {
    await FirebaseManager.shared.firestore
        .collection('tasks')
        .doc(task.id)
        .delete();
  }

  Future<void> updateTask(Task task) async {
    await FirebaseManager.shared.firestore
        .collection("tasks")
        .doc(task.id)
        .update(task.toJson());
  }
}
