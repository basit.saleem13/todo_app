import 'package:get_it/get_it.dart';
import 'package:morphosis_flutter_demo/non_ui/repo/home/home_repository.dart';
import 'package:morphosis_flutter_demo/non_ui/repo/task/task_repositoy.dart';

// This is our global ServiceLocator
GetIt getIt = GetIt.instance;

setUpLocator() {
  // this is used to perform CURD operations
  getIt.registerLazySingleton<TaskRepository>(() => TaskRepository());
  //this is used to perform API request
  getIt.registerLazySingleton<HomeRepository>(() => HomeRepository());
}
