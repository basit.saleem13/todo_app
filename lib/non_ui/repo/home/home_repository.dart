import 'package:morphosis_flutter_demo/non_ui/modal/album.dart';
import 'package:morphosis_flutter_demo/non_ui/repo/network_manager.dart';
import 'package:morphosis_flutter_demo/non_ui/utils/api_constants.dart';

class HomeRepository {
  @override
  Future<List<Album>> getFacts() async {
    List<Album> facts = [];
    var res =
        await NetworkProvider.instance().get('${ApiConstant.baseUrl}/albums');
    if (res.statusCode == 200 && res.data != null) {
      facts = List<Album>.from(res.data.map((x) => Album.fromJson(x)));
      return facts;
    }
    return facts;
  }
}
