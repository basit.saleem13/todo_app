import 'package:dio/dio.dart';

class NetworkProvider {
  String authorization;

  static Dio instance({String authorization, String contentType}) {
    final dio = Dio();
    //authorization=authorization;
    //  dio.options.contentType = contentType ?? Headers.formUrlEncodedContentType;
    // dio.options.headers["language"] = "en";
    // dio.options.headers['Authorization'] = authorization;
    //  dio.options.headers['Content-Type']= contentType ?? "application/x-www-form-urlencoded";
    // dio.options.headers['Accept'] = "application/json";
    //dio.options.connectTimeout = 10000;
    // dio.interceptors.add(LogInterceptor(requestBody: true,responseBody: true));

    return dio;
  }
}

class HttpLogInterceptor extends InterceptorsWrapper {}

enum DioErrorType {
  /// When opening  url timeout, it occurs.
  CONNECT_TIMEOUT,

  ///It occurs when receiving timeout.
  RECEIVE_TIMEOUT,

  /// When the server response, but with a incorrect status, such as 404, 503...
  RESPONSE,

  /// When the request is cancelled, dio will throw a error with this type.
  CANCEL,

  /// Default error type, Some other Error. In this case, you can
  /// read the DioError.error if it is not null.
  DEFAULT,
}
