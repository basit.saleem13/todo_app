import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:morphosis_flutter_demo/non_ui/modal/album.dart';

Future<bool> isInternetConnected() async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile) {
    return true;
  } else if (connectivityResult == ConnectivityResult.wifi) {
    return true;
  }
  return false;
}

String encode(List<Album> facts) => json.encode(
      facts.map<Map<String, dynamic>>((fact) => fact.toJson()).toList(),
    );

List<Album> decode(String facts) => (json.decode(facts) as List<dynamic>)
    .map<Album>((item) => Album.fromJson(item))
    .toList();
