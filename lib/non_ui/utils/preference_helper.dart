import 'dart:convert';

import 'package:morphosis_flutter_demo/non_ui/modal/album.dart';
import 'package:morphosis_flutter_demo/non_ui/utils/helper_functions.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferencesHelper {
  static SharedPreferences prefs;

  static Future<dynamic> _getInstance() async =>
      prefs = await SharedPreferences.getInstance();

  static Future<bool> savePreferences(String key, String data) async {
    await _getInstance();
    return prefs.setString(key, data);
  }

  static Future<bool> deletePreferences(String key) async {
    await _getInstance();
    return prefs.remove(key);
  }

  static Future<String> getPreferences(String key) async {
    await _getInstance();
    return prefs.getString(key) ?? null;
  }

  static Future<bool> saveFacts(List<Album> facts) async {
    await _getInstance();
    final String encodedData = encode(facts);
    return await prefs.setString(PreferencesKey.facts, encodedData);
  }

  static Future<List<Album>> getFacts() async{
    await _getInstance();
    final String encodedData = prefs.getString(PreferencesKey.facts);
    return decode(encodedData);
  }
}


class PreferencesKey {
  static String facts = "facts";
}
